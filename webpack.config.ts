import * as path from 'path';
import webpack from 'webpack';
import CopyPlugin from 'copy-webpack-plugin';

const sourcePath = path.resolve(__dirname, 'src');
const staticPath = path.resolve(__dirname, 'static');
const artifactPath = path.resolve(__dirname, 'artifacts');

export const config: webpack.Configuration = {
  mode: 'development',
  entry: {
    content_script: path.resolve(sourcePath, 'content_script', 'index.ts'),
    background: path.resolve(sourcePath, 'background', 'index.ts'),
  },
  output: {
    path: path.resolve(__dirname, 'artifacts'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: 'ts-loader',
        exclude: '/node_modules',
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  plugins: [
    new CopyPlugin({
      patterns: [{ from: staticPath, to: artifactPath }],
    }),
  ],
};

const modes = ['production', 'development', 'none'] as const;
type Mode = typeof modes[number];

const isMode = (mode: any): mode is Mode =>
  ['production', 'development', 'none'].includes(mode);

if (isMode(process.env.NODE_ENV)) {
  config.mode = process.env.NODE_ENV;
}

export default config;
