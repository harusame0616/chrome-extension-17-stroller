const modes = ['idle', 'running'] as const;
type Mode = typeof modes[number];
let mode: Mode = 'idle';

chrome.action.onClicked.addListener((tab) => {
  if (!tab.id) {
    return;
  }

  chrome.tabs.sendMessage(
    tab.id,
    mode == 'idle' ? 'start' : 'stop',
    (response) => {
      if (!response) {
        return;
      }

      if (mode == 'idle') {
        mode = 'running';
      } else {
        mode = 'idle';
      }
    }
  );
});
