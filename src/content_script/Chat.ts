import { v4 } from 'uuid';
const chatTypes = ['comment', 'gift', 'share', 'nice', 'lucky'] as const;
type ChatType = typeof chatTypes[number];
const isChatType = (chatType: any): chatType is ChatType => {
  return chatTypes.includes(chatType);
};

export interface ChatParam {
  level: number;
  name: string;
  content: string;
  type: ChatType;
  id: string;
  createdAt: Date;
}

export class Chat {
  private _level;
  private _name;
  private _content;
  private _type;
  private _id;
  private _createdAt;

  constructor({ level, name, content, type, id, createdAt }: ChatParam) {
    this._level = level;
    this._name = name;
    this._content = content;
    this._type = type;
    this._id = id;
    this._createdAt = createdAt;
  }

  get level() {
    return this._level;
  }
  get name() {
    return this._name;
  }
  get content() {
    return this._content;
  }
  get isNice() {
    return this._type === 'nice';
  }
  get isShare() {
    return this._type === 'share';
  }
  get isGift() {
    return this._type === 'gift';
  }
  get isComment() {
    return this._type === 'comment';
  }
  get giftName() {
    if (!this.isGift) {
      return undefined;
    }

    const [_, giftName] =
      this.content.match(
        /(.*?) をライバーにプレゼントします|(.*?) を開けました/
      ) || [];

    return giftName;
  }

  get type() {
    return this._type;
  }

  get id() {
    return this._id;
  }

  get createdAt() {
    return this._createdAt;
  }

  static create(node: Node) {
    const level = (node as Element).querySelector('[class*=LevelBadge]');
    const name = (node as Element).querySelector('[class*=ChatUserName]');
    const content = (node as Element).querySelector(
      '[class*=Chat__ContentWrapper]'
    );
    const reaction = content?.querySelector(
      '[class*="Reaction__ControlledText"'
    );

    let type = 'comment';
    if (reaction?.textContent == 'ライバーにいいねしました') {
      type = 'nice';
    } else if (reaction?.textContent?.includes('シェア')) {
      type = 'share';
    } else if (content?.querySelector('[class*="GiftItem"')) {
      type = 'gift';
    } else if (content?.querySelector('[class*="SystemComment"')) {
      type = 'lucky';
    }

    let nameText = name?.textContent ?? '自分';
    if (!level?.textContent) {
      // ラッキーの時はレベルのdomなし
      if (type === 'lucky') {
        nameText = '17LIVE';
      } else if (type === 'gift') {
        // ほっぺにちゅー
        // do nothing
      } else {
        console.error(node);
        throw new Error('レベルが見つかりません');
      }
    }

    if (!content?.textContent) {
      console.error(node);
      throw new Error('コンテンツが見つかりません');
    }

    if (!isChatType(type)) {
      console.error(node);
      throw new Error('チャットタイプが不明です');
    }

    return new Chat({
      level: Number(level?.textContent ?? '0'),
      name: nameText,
      content: content.textContent,
      type,
      id: v4(),
      createdAt: new Date(),
    });
  }
}
