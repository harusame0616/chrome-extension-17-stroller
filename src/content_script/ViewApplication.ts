type ViewElement = HTMLElement | Node;

export interface ConstructorParam {
  giftTotalElement: ViewElement;
  giftUniqueElement: ViewElement;
  shareTotalElement: ViewElement;
  shareUniqueElement: ViewElement;
}

export interface RefreshParam {
  giftTotal: number | string;
  giftUnique: number | string;
  shareUnique: number | string;
  shareTotal: number | string;
}

export class ViewApplication {
  private _giftTotalElement: ViewElement;
  private _giftUniqueElement: ViewElement;
  private _shareTotalElement: ViewElement;
  private _shareUniqueElement: ViewElement;

  constructor({
    giftTotalElement,
    giftUniqueElement,
    shareTotalElement,
    shareUniqueElement,
  }: ConstructorParam) {
    this._giftTotalElement = giftTotalElement as HTMLElement;
    this._giftUniqueElement = giftUniqueElement as HTMLElement;
    this._shareTotalElement = shareTotalElement as HTMLElement;
    this._shareUniqueElement = shareUniqueElement as HTMLElement;
  }

  refresh(param: RefreshParam) {
    this._giftTotalElement.textContent = `${param.giftTotal}`;
    this._giftUniqueElement.textContent = `${param.giftUnique}`;
    this._shareTotalElement.textContent = `${param.shareTotal}`;
    this._shareUniqueElement.textContent = `${param.shareUnique}`;
  }
}
