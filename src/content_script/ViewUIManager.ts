export interface ViewUIManager {
  setup(): {
    giftTotalElement: HTMLElement;
    giftUniqueElement: HTMLElement;
    shareTotalElement: HTMLElement;
    shareUniqueElement: HTMLElement;
    giftTotalPlusButton: HTMLElement;
    giftTotalMinusButton: HTMLElement;
    giftUniuqePlusButton: HTMLElement;
    giftUniuqeMinusButton: HTMLElement;
    shareTotalPlusButton: HTMLElement;
    shareTotalMinusButton: HTMLElement;
    shareUniuqePlusButton: HTMLElement;
    shareUniuqeMinusButton: HTMLElement;
    downloadButton: HTMLButtonElement;
  };
  reset(): void;
}
