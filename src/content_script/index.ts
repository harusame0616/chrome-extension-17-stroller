import dayjs from 'dayjs';
import { Chat, ChatLog } from './ChatLog';
import { ChatObserver } from './ChatObserveApplication';
import { SuggestedAreaViewUIManager } from './SuggestedAreaViewUIManager';
import { ViewApplication } from './ViewApplication';
import { ViewUIManager } from './ViewUIManager';

let chatLog: ChatLog;
let viewApplication: ViewApplication;
let viewUIManager: ViewUIManager = new SuggestedAreaViewUIManager();
let totalGiftOffset = 0;
let uniqueGiftOffset = 0;
let totalShareOffset = 0;
let uniqueShareOffset = 0;

let chatObserver = new ChatObserver((chats: Chat[]) => {
  chats.forEach((chat) => chatLog.addChat(chat));

  refresh();
});

let refresh = () => {
  viewApplication.refresh({
    giftTotal: `${chatLog.countGift() + totalGiftOffset} (${totalGiftOffset})`,
    giftUnique: `${
      chatLog.countUniqueGift() + uniqueGiftOffset
    } (${uniqueGiftOffset})`,
    shareUnique: `${
      chatLog.countUniqueShare() + uniqueShareOffset
    } (${uniqueShareOffset})`,
    shareTotal: `${
      chatLog.countShare() + totalShareOffset
    } (${totalShareOffset})`,
  });
};

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request == 'start') {
    // UI作成
    const {
      giftTotalElement,
      giftUniqueElement,
      shareTotalElement,
      shareUniqueElement,
      downloadButton,
      giftTotalMinusButton,
      giftTotalPlusButton,
      giftUniuqeMinusButton,
      giftUniuqePlusButton,
      shareTotalMinusButton,
      shareTotalPlusButton,
      shareUniuqeMinusButton,
      shareUniuqePlusButton,
    } = viewUIManager.setup();

    // UIレンダリング開始
    viewApplication = new ViewApplication({
      giftTotalElement: giftTotalElement,
      giftUniqueElement: giftUniqueElement,
      shareTotalElement: shareTotalElement,
      shareUniqueElement: shareUniqueElement,
    });

    // チャットリストを監視してログの更新と画面のリフレッシュ
    chatObserver.observe();

    // ダウンロードボタンによるダウンロード処理
    downloadButton.onclick = () => {
      if (chatLog) {
        download();
      }
    };

    giftTotalMinusButton.onclick = () => {
      totalGiftOffset--;
      refresh();
    };
    giftTotalPlusButton.onclick = () => {
      totalGiftOffset++;
      refresh();
    };
    giftUniuqeMinusButton.onclick = () => {
      uniqueGiftOffset--;
      refresh();
    };
    giftUniuqePlusButton.onclick = () => {
      uniqueGiftOffset++;
      refresh();
    };
    shareTotalMinusButton.onclick = () => {
      totalShareOffset--;
      refresh();
    };
    shareTotalPlusButton.onclick = () => {
      totalShareOffset++;
      refresh();
    };
    shareUniuqeMinusButton.onclick = () => {
      uniqueShareOffset--;
      refresh();
    };
    shareUniuqePlusButton.onclick = () => {
      uniqueShareOffset++;
      refresh();
    };

    chatLog = new ChatLog();
    sendResponse(true);
  } else {
    viewUIManager.reset();
    chatObserver?.disconnect();
    sendResponse(true);
  }
});

const download = () => {
  const data = [
    'id,date,time,type,level,name,content,gift',
    ...Array.from(chatLog).map((chat) => [
      chat.id,
      dayjs(chat.createdAt).format('YYYY-MM-DD'),
      dayjs(chat.createdAt).format('HH:mm:ss'),
      chat.type,
      chat.level,
      chat.name,
      `"${chat.content.replace('"', '""')}"`,
      chat.giftName,
    ]),
  ].join('\n');

  var a = document.createElement('a');
  a.href = window.URL.createObjectURL(new Blob([data], { type: 'text/csv' }));
  document.body.appendChild(a);
  a.download = `chat_log_${dayjs().format('YYYY-MM-DDHHmm')}.csv`;
  a.click();
  a.remove();
};
