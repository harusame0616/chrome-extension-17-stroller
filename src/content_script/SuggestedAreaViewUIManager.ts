import { ViewUIManager } from './ViewUIManager';

const strollerDomChildParams = [
  ['トータルギフト数', 'total-gift-count'],
  ['ユニークギフト数', 'unique-gift-count'],
  ['トータルシェア数', 'total-share-count'],
  ['ユニークシェア数', 'unique-share-count'],
];

export class SuggestedAreaViewUIManager implements ViewUIManager {
  private _rootDom: HTMLElement;
  private _titleDom: HTMLElement;
  private _viewElements: [HTMLElement, HTMLElement, HTMLElement, HTMLElement];
  private _valueElements: HTMLElement[] = [];
  private _downloadButton: HTMLButtonElement;
  private _totalGiftAdditionButton = document.createElement('button');
  private _uniqueGiftAdditionButton = document.createElement('button');
  private _totalShareAdditionButton = document.createElement('button');
  private _uniqueShareAdditionButton = document.createElement('button');

  constructor() {
    this._rootDom = document.createElement('div');
    this._rootDom.className = 'stroller';

    this._titleDom = document.createElement('span');
    this._titleDom.textContent = '17 Stroller';
    this._downloadButton = document.createElement('button');
    this._downloadButton.textContent = 'Download log';
    this._downloadButton.className = 'download';

    this._viewElements = strollerDomChildParams.map(([label, className]) => {
      const wrapperElement = document.createElement('div');
      wrapperElement.className = `${className} item-wrapper`;

      const labelElement = document.createElement('span');
      labelElement.textContent = label;
      labelElement.className = `${className} item-label`;
      const valueElement = document.createElement('span');
      this._valueElements.push(valueElement);
      valueElement.textContent = '0';
      valueElement.className = `${className} item-value`;

      const plusButton = document.createElement('button', {});
      plusButton.textContent = '+';
      this._valueElements.push(plusButton);
      const minusButton = document.createElement('button', {});
      minusButton.textContent = '-';
      this._valueElements.push(minusButton);
      [labelElement, valueElement, minusButton, plusButton].forEach((element) =>
        wrapperElement.appendChild(element)
      );
      return wrapperElement as HTMLElement;
    }) as typeof this._viewElements;
  }

  setup() {
    // タイトル部設定
    const panelTitle = document.querySelector(
      '[class*="SuggestedPanel__PanelTitle"]'
    ) as HTMLElement;
    panelTitle.appendChild(this._titleDom);
    (panelTitle.firstChild as HTMLElement).style.display = 'none';

    this._viewElements.forEach((element) => {
      this._rootDom?.appendChild(element);
    });

    this._rootDom.appendChild(this._downloadButton);

    const displayArea = document.querySelector(
      '[class*="SuggestedPanel__PanelWrapper"]'
    );
    const panelContent = document.querySelector(
      '[class*="SuggestedPanel__PanelContent"]'
    ) as HTMLElement;
    panelContent.style.display = 'none';
    if (!displayArea) {
      throw new Error('MainWrapperが見つかりません');
    }
    displayArea.appendChild(this._rootDom);

    const [
      giftTotalElement,
      giftTotalPlusButton,
      giftTotalMinusButton,
      giftUniqueElement,
      giftUniuqePlusButton,
      giftUniuqeMinusButton,
      shareTotalElement,
      shareTotalPlusButton,
      shareTotalMinusButton,
      shareUniqueElement,
      shareUniuqePlusButton,
      shareUniuqeMinusButton,
    ] = this._valueElements;

    return {
      giftTotalElement,
      giftTotalPlusButton,
      giftTotalMinusButton,
      giftUniqueElement,
      giftUniuqePlusButton,
      giftUniuqeMinusButton,
      shareTotalElement,
      shareTotalPlusButton,
      shareTotalMinusButton,
      shareUniqueElement,
      shareUniuqePlusButton,
      shareUniuqeMinusButton,
      downloadButton: this._downloadButton,
    };
  }

  reset(): void {
    this._rootDom.remove();
    this._titleDom.remove();
    const panelTitle = document.querySelector(
      '[class*="SuggestedPanel__PanelTitle"]'
    ) as HTMLElement;
    (panelTitle.firstChild as HTMLElement).style.display = 'block';

    const panelContent = document.querySelector(
      '[class*="SuggestedPanel__PanelContent"]'
    );
    (panelContent as HTMLElement).style.display = '';
  }
}
