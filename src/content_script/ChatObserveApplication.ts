import { Chat } from './Chat';

export class ChatObserver {
  private _observer: MutationObserver;
  constructor(observer: any) {
    this._observer = new MutationObserver(
      (mutationRecords: MutationRecord[]) => {
        const chats = mutationRecords.flatMap((mutationRecord) =>
          Array.from(mutationRecord.addedNodes).map((node) => Chat.create(node))
        );

        observer(chats);
      }
    );
  }

  observe() {
    const chatDom = document.querySelector('[class*="ChatList__ListWrapper"]');

    if (!chatDom) {
      throw new Error('チャットが見つかりません');
    }

    this._observer.observe(chatDom, { childList: true });
  }

  disconnect() {
    this._observer.disconnect();
  }
}
