import { Chat } from './Chat';

export { Chat };

export class ChatLog {
  chats: Chat[] = [];
  constructor() {}

  addChat(chat: Chat) {
    if (this.chats.map((chat) => chat.id).includes(chat.id)) {
      throw new Error('登録ずみのチャットです');
    }
    this.chats.push(chat);
  }

  countUniqueGift() {
    return new Set(
      this.chats.filter((chat) => chat.isGift).map((chat) => chat.name)
    ).size;
  }

  countGift() {
    return this.chats.filter((chat) => chat.isGift).length;
  }

  countUniqueShare() {
    return new Set(
      this.chats.filter((chat) => chat.isShare).map((chat) => chat.name)
    ).size;
  }

  countShare() {
    return this.chats.filter((chat) => chat.isShare).length;
  }

  get leastChat() {
    return this.chats.slice(-1)?.[0];
  }

  *[Symbol.iterator]() {
    const chats = this.chats;
    for (const chat of chats) {
      yield chat;
    }
  }
}
